locals {
  cpu = {
    dev  = 512
    prod = 2048
  }
  memory = {
    dev  = 1024
    prod = 2048
  }
}
