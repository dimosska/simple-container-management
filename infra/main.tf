terraform {

  backend "s3" {
    bucket         = "simple-container-management-state-bucket"
    key            = "terraform.tfstate"
    region         = "eu-west-3"
    dynamodb_table = "terraform-lock-table"

  }

  required_providers {
    aws = {
      version = "~> 4.0"
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "eu-west-3"
}

