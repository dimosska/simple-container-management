resource "aws_s3_bucket" "tfstate-bucket" {
  bucket = "simple-container-management-state-bucket"
  tags = {
    Name = "Remote tfstate S3 bucket"
  }
}
resource "aws_s3_bucket_versioning" "tfstate-bucket-versioning" {
  bucket = aws_s3_bucket.tfstate-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_server_side_encryption_configuration" "tfstate-encryption" {
  bucket = aws_s3_bucket.tfstate-bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
resource "aws_s3_bucket_object_lock_configuration" "tfstate-object-lock-config" {
  bucket              = aws_s3_bucket.tfstate-bucket.id
  object_lock_enabled = "Enabled"
}
resource "aws_dynamodb_table" "terraform-lock" {
  name           = "terraform-lock-table"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "DynamoDB Terraform lock table"
  }
}
