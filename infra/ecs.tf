resource "aws_ecs_cluster" "ecs-cluster" {
  name = "${terraform.workspace}-webapp-cluster"
}


resource "aws_ecs_task_definition" "service" {
  family                   = "service"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = local.cpu[terraform.workspace]
  memory                   = local.memory[terraform.workspace]
  execution_role_arn       = data.aws_iam_role.task-execution-role.arn
  task_role_arn            = aws_iam_role.ecs-task-role.arn
  container_definitions = jsonencode([
    {
      image      = "${aws_ecr_repository.my-ecr-repo.repository_url}:latest"
      name       = "web-server-${terraform.workspace}"
      entrypoint = []
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      healthCheck = {
        retries = 10
        command = ["CMD-SHELL", "curl -f http://localhost:80 || exit 1"]
        timeout : 5
        interval : 60
      }
    }
  ])
}

resource "aws_ecs_service" "web-service" {
  name            = "web-service"
  cluster         = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.service.id
  desired_count   = 1
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type          = "FARGATE"
  force_new_deployment = true
  network_configuration {
    subnets          = [aws_subnet.private-subnet-a.id, aws_subnet.private-subnet-b.id]
    assign_public_ip = true
    security_groups  = [aws_security_group.allow_http.id, aws_security_group.open.id]
  }


  load_balancer {
    target_group_arn = aws_lb_target_group.target-group.id
    container_name   = "web-server-${terraform.workspace}"
    container_port   = 80
  }
}
