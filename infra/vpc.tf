resource "aws_vpc" "my-vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    Name = "main-igw"
  }
}

# Declare the data source
data "aws_availability_zones" "available" {}

resource "aws_subnet" "public-subnet-a" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "Public-subnet-a"
  }
}

resource "aws_subnet" "public-subnet-b" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "Public-subnet-b"
  }
}

resource "aws_subnet" "private-subnet-a" {
  vpc_id            = aws_vpc.my-vpc.id
  cidr_block        = "10.0.101.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "Private-subnet-a"
  }
}

resource "aws_subnet" "private-subnet-b" {
  vpc_id            = aws_vpc.my-vpc.id
  cidr_block        = "10.0.102.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "Private-subnet-b"
  }
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.my-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-igw.id
  }

  tags = {
    Name = "internet-access"
  }
}
resource "aws_route_table_association" "public-a-rta" {
  subnet_id      = aws_subnet.public-subnet-a.id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_route_table_association" "public-b-rta" {
  subnet_id      = aws_subnet.public-subnet-b.id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_security_group" "open" {
  name        = "open"
  description = "open group"
  vpc_id      = aws_vpc.my-vpc.id

  ingress {
    description = "fully open group"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.my-vpc.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}





resource "aws_eip" "eip-a" {
}

resource "aws_eip" "eip-b" {
}



resource "aws_route_table_association" "private-rta-a" {
  subnet_id      = aws_subnet.private-subnet-a.id
  route_table_id = aws_route_table.private-route-table-a.id
}

resource "aws_route_table_association" "private-rta-b" {
  subnet_id      = aws_subnet.private-subnet-b.id
  route_table_id = aws_route_table.private-route-table-b.id
}

resource "aws_nat_gateway" "nat-a" {
  subnet_id     = aws_subnet.public-subnet-a.id
  allocation_id = aws_eip.eip-a.id
  depends_on    = [aws_internet_gateway.my-igw]
}

resource "aws_nat_gateway" "nat-b" {
  subnet_id     = aws_subnet.public-subnet-b.id
  allocation_id = aws_eip.eip-b.id
  depends_on    = [aws_internet_gateway.my-igw]
}

resource "aws_route_table" "private-route-table-a" {
  vpc_id = aws_vpc.my-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-a.id
  }
}

resource "aws_route_table" "private-route-table-b" {
  vpc_id = aws_vpc.my-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-b.id
  }
}
