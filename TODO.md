### TODO list

- [x] Change network to private -> Public+NAT -> IGW

# Add some pipelines to CI
- [x] execute tf code from GitlabCI
- [x] create and push Docker image to AWS
- [ ] **add tag with semantic release**
# Add another application (e.g. FastAPI) to check health
- [ ] Deploy this application to AWS. OR create EKS cluster with 2 apps.

